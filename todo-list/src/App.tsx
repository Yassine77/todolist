import { FC, ChangeEvent, useState } from "react";

import './App.css';
import TodoTask from'./todoList'

export interface Itask {
  name:string,
  deadline:number
}

const App: React.FC = ()=> {
  const [test2 , settest2] = useState<boolean>(false)
  const [task , setTask] = useState<string>('')
  const [deadline , setDealine] = useState<number>(0)
  const [todoList , todolistkHandler] = useState<Itask[]>([])

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    if (event.target.name === "task") {
      setTask(event.target.value);
    } else {
      setDealine(Number(event.target.value));
    }
  };

  const addTask =() : void =>{
       const newTask={name:task,deadline:deadline}
       todolistkHandler ([...todoList , newTask])
       setTask('')
       setDealine(0)
  }

  const deleateTask = (name:string) =>{
    todolistkHandler (
      todoList.filter(task =>{
        return(
          task.name !== name
        )
      })
    )
  }


  return (
    <div className="App">
      <div className="header">
        <div className="inputContainer">
          <input
            type="text"
            placeholder="Task..."
            name="task"
            value={task}
            onChange={handleChange}
          />
          <input
            type="number"
            placeholder="Deadline (in Days)..."
            name="deadline"
            value={deadline}
            onChange={handleChange}
          />
        </div>
        <button onClick={addTask} >Add Task</button>
      </div >
      <div className="todoList">
        {todoList.map((task:Itask) =>{
          return(
            <TodoTask task={task} />
          )
        })}
      </div>
    </div>
  );
};

export default App;
