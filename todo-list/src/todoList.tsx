import React, { FC, ChangeEvent, useState } from "react";
import { Itask } from "./App";

interface IProps{
    task: Itask,
    deleteTask?:void
}

const TodoTask: React.FC<IProps>= ({task,deleteTask})=>{
    return(
        <div className="task">
            <div className="content">
                <span>{task.name}</span>
                <span>{task.deadline}</span>
            </div>
            <button 
                >x
            </button>
        </div>
        )
;}
export default TodoTask;